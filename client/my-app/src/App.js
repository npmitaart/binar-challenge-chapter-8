import { BrowserRouter, Redirect, Route, Switch } from "react-router-dom";
import Home from "./endpoints/homePage";
import FormAddPlayer from "./endpoints/FormAddPlayer";
import NotFound from "./endpoints/notFound404";

function App() {
    return (
        <BrowserRouter>
            <div className="container">
                <Switch>
                    <Route
                        path="/"
                        exact
                        render={() => <Redirect to="/players" />}
                    />
                    {/* edit here untuk render */}
                    <Route path="/players" exact render={Home} />
                    <Route path="/players/add" exact render={FormAddPlayer} />
                    <Route component={NotFound} />
                </Switch>
            </div>
        </BrowserRouter>
    );
}

export default App;
