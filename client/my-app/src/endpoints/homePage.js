import React from "react";
import { Link } from "react-router-dom";

const Home = () => {
    return (
        <div className="card mt-5">
            <div className="card-body pt-4 pb-5">
                <div
                    className="row mb-3"
                    style={{ borderBottom: "2px solid rgba(0,0,0,10)" }}
                >
                    <div className="col-12 col-md-10">
                        <h1>Players Data</h1>
                    </div>
                </div>
                <div className="row">
                    <div className="col table-responsive">
                        <table className="table">
                            <thead>
                                <tr className="bg-info">
                                    <th>Index</th>
                                    <th>Username</th>
                                    <th>Email</th>
                                    <th>Experience</th>
                                    <th>Level</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Giodio Mitaart</td>
                                    <td>giodio123@gmail.com</td>
                                    <td>20</td>
                                    <td>2</td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Tama</td>
                                    <td>Tamagochi@gmail.com</td>
                                    <td>100</td>
                                    <td>10</td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Dummy</td>
                                    <td>Dummy@gmail.com</td>
                                    <td>0</td>
                                    <td>0</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div className="col-4 col-md-2">
                        <Link
                            to="/players/add"
                            className="btn btn-block btn-md btn-primary"
                        >
                            Add New Player
                        </Link>
                    </div>
            </div>
        </div>
    );
};

export default Home;
