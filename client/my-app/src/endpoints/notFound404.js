import React from "react";
import { Link } from "react-router-dom";

const NotFound = () => {
    return (
        <div style={{ height: "100vh" }}>
            <div className="h-100 d-flex justify-content-center align-items-center">
                <div>
                    <h1>404 Not Found</h1>
                    <h4>Sorry the page that you're looking for doesn't exist!</h4>
                    <Link to="/" class="btn btn-sm btn-warning">
                        Back to player page
                    </Link>
                </div>
            </div>
        </div>
    );
};
export default NotFound;
