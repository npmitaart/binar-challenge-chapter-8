import React from "react";

const FormAddPlayer = () => {
    return (
        
        <div className="card mt-5">
            <div className="card-body pt-4 pb-5">
                <div
                    className="row mb-3"
                    style={{ borderBottom: "2px solid rgba(0,0,0,10)" }}
                >
                    <div className="col">
                        <h1>Add Players</h1>
                    </div>
                </div>
                <div className="row">
                    <div className="col col-md-6 offset-md-3">
                        <form>
                            <div class="form-group">
                                <label for="username">Username</label><br></br>
                                <input
                                    type="text"
                                    class="form-control"
                                    id="username"
                                    name="username"
                                    placeholder="Username"
                                />
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label><br></br>
                                <input
                                    type="email"
                                    class="form-control"
                                    id="email"
                                    name="email"
                                    placeholder="Email"
                                />
                            </div>
                            <div class="form-group">
                                <label for="password">Password</label><br></br>
                                <input
                                    type="password"
                                    class="form-control"
                                    id="password"
                                    name="password"
                                    placeholder="Password"
                                />
                            </div>
                            <div class="form-group"> 
                                <label for="exp">Experience</label><br></br>
                                <input
                                    type="number"
                                    class="form-control"
                                    id="exp"
                                    name="exp"
                                    placeholder="Experience"
                                />
                            </div>
                            <br></br>
                            <button
                                type="submit"
                                class="btn btn-block btn-primary"
                            >
                                Submit
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default  FormAddPlayer;

// import React from 'react';
// import { useForm } from 'react-hook-form';

// export default function FormAddPlayer() {
//   const { register, handleSubmit, errors } = useForm();
//   const onSubmit = data => console.log(data);
//   console.log(errors);
  
//   return (
//     <form onSubmit={handleSubmit(onSubmit)}>
//       <input type="text" placeholder="Username" name="Username" ref={register} />
//       <input type="email" placeholder="Email" name="Email" ref={register({required: true})} />
//       <input type="tel" placeholder="Mobile number" name="Mobile number" ref={register({required: true, minLength: 6, maxLength: 12})} />
//       <select name="Title" ref={register({ required: true })}>
//         <option value="Mr">Mr</option>
//         <option value="Mrs">Mrs</option>
//         <option value="Miss">Miss</option>
//         <option value="Dr">Dr</option>
//       </select>
//       <input type="number" placeholder="Experience" name="Experience" ref={register} />

//       <input type="submit" />
//     </form>
//   );
// }